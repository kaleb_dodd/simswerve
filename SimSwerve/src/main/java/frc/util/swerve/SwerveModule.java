package frc.util.swerve;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.AnalogInput;
import frc.io.RobotOutput;
import frc.util.SimPID;

public class SwerveModule {

    private CANSparkMax rotationMotor;
    private CANSparkMax wheelMotor;
    private AnalogInput rotationEnc;
    private SimPID rotationPID; 
    private static int TICKS_PER_REV = 4096; // find value 
    

    public SwerveModule(int rotationMotorPort,int wheelMotorPort,int encPort, SimPID rotationPID){
        this.rotationMotor = new CANSparkMax(rotationMotorPort, MotorType.kBrushless);
        this.wheelMotor = new CANSparkMax(wheelMotorPort, MotorType.kBrushless);
        this.rotationEnc = new AnalogInput(encPort);
        this.rotationPID = rotationPID;
    }
 

    public void set(double angle, double speed) {
        // don't reset wheel azimuth direction to zero when returning to neutral
        if (speed == 0) {
            this.wheelMotor.set(speed);
            return;
        }
  
        //angle *= -TICKS_PER_REV; // flip azimuth, hardware configuration dependent
  
        double azimuthPosition = this.rotationEnc.getVoltage();
        double azimuthError = Math.IEEEremainder(angle - azimuthPosition, TICKS_PER_REV);
  
        // minimize azimuth rotation, reversing drive if necessary
        boolean isInverted = Math.abs(azimuthError) > 0.25 * TICKS_PER_REV;
        if (isInverted) {
            azimuthError -= Math.copySign(0.5 * TICKS_PER_REV, azimuthError);
            speed = -speed;
        }

        this.rotationPID.setDesiredValue(azimuthPosition + azimuthError);
        this.rotationMotor.set(rotationPID.calcPID(azimuthPosition));
        this.wheelMotor.set(speed);
    }

    public void setAngle(double angle){
        double azimuthPosition = this.rotationEnc.getVoltage();
        this.rotationPID.setDesiredValue(angle);
        this.rotationMotor.set(rotationPID.calcPID(azimuthPosition));
    }


    public double getAngle(){
        return this.rotationEnc.getAverageVoltage();
    }

    public double getWheelEnc(){
        return this.wheelMotor.getEncoder().getPosition();
    }

    public double getWheelEncSpeed(){
        return this.wheelMotor.getEncoder().getVelocity();
    }

    public void zeroWheelEnc(){
        this.wheelMotor.getEncoder().setPosition(0);
    }
    

    

   
   

  
}