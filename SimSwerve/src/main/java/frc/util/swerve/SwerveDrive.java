package frc.util.swerve;

import frc.util.PIDConstants;
import frc.util.SimNavx;
import frc.util.SimPID;
import frc.util.Vect;

//Wheels are a array numbered 0-3 from front to back, with even numbers on the left side when
//  facing forward.
public class SwerveDrive {

    private SimNavx gyro; 
    private double lengthComponent;
    private double widthComponent; 
    private int wheelCount = 4;
    private SwerveModule[] modules = new SwerveModule[wheelCount];; 
    private double[] wheelSpeeds = new double[wheelCount];
    private double[] wheelAngles = new double[wheelCount];
    private boolean isFieldOriented = true;
    private Vect position = new Vect(0,0); 

    public SwerveDrive(int[] steeringPorts, int[] drivingPorts,
        int[] encoderPorts, PIDConstants steeringPIDConstants, double length, double width){
            
            for(int i =0; i < wheelCount; i++){
                this.modules[i] = new SwerveModule(steeringPorts[i], drivingPorts[i], encoderPorts[i], new SimPID(steeringPIDConstants));
            }

            double radius = Math.hypot(length, width);
            this.lengthComponent = length / radius;
            this.widthComponent = width / radius; 

            this.gyro = new SimNavx();
            
    }

    public void set(double angle, double speed){
        for (int i = 0; i < wheelCount; i++){
            this.modules[i].set(angle, speed);
        }
    }

    /**
   * Drive the robot in given field-relative direction and with given rotation.
   *
   * @param forward Y-axis movement, from -1.0 (reverse) to 1.0 (forward)
   * @param strafe X-axis movement, from -1.0 (left) to 1.0 (right)
   * @param azimuth robot rotation, from -1.0 (CCW) to 1.0 (CW)
   */
  public void drive(double forward, double strafe, double azimuth) {

    // Use gyro for field-oriented drive. We use getAngle instead of getYaw to enable arbitrary
    // autonomous starting positions.
    if (isFieldOriented) {
      double angle = gyro.getAngle();
      angle = Math.IEEEremainder(angle, 360.0);

      angle = Math.toRadians(angle);
      final double temp = forward * Math.cos(angle) + strafe * Math.sin(angle);
      strafe = strafe * Math.cos(angle) - forward * Math.sin(angle);
      forward = temp;
    }

    final double a = strafe - azimuth * lengthComponent;
    final double b = strafe + azimuth * lengthComponent;
    final double c = forward - azimuth * widthComponent;
    final double d = forward + azimuth * widthComponent;

    // wheel speed
    wheelSpeeds[0] = Math.hypot(b, d);
    wheelSpeeds[1] = Math.hypot(b, c);
    wheelSpeeds[2] = Math.hypot(a, d);
    wheelSpeeds[3] = Math.hypot(a, c);

    // wheel azimuth
    wheelAngles[0] = Math.atan2(b, d) * 0.5 / Math.PI;
    wheelAngles[1] = Math.atan2(b, c) * 0.5 / Math.PI;
    wheelAngles[2] = Math.atan2(a, d) * 0.5 / Math.PI;
    wheelAngles[3] = Math.atan2(a, c) * 0.5 / Math.PI;

    // normalize wheel speed
    final double maxWheelSpeed = Math.max(Math.max(wheelSpeeds[0], wheelSpeeds[1]), Math.max(wheelSpeeds[2], wheelSpeeds[3]));
    if (maxWheelSpeed > 1.0) {
      for (int i = 0; i < wheelCount; i++) {
        wheelSpeeds[i] /= maxWheelSpeed;
      }
    }

    // set wheels
    for (int i = 0; i < wheelCount; i++) {
        modules[i].set(wheelAngles[i], wheelSpeeds[i]);
    }
  }

  public void updatePosition(double deltaTime){
      
        Vect dPosition = new Vect(0,0);

        for(int i = 0; i < wheelCount; i++){
            dPosition = dPosition.add(new Vect(modules[i].getWheelEncSpeed() * deltaTime, modules[i].getAngle(),0));
        }

        dPosition = dPosition.rotate(this.gyro.getAngle());
        this.position = this.position.add(dPosition);
  }

  public double getDriveAngle(){
      return this.gyro.getAngle();
  }






}
