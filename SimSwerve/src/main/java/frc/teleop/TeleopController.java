package frc.teleop;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.imaging.SimLimelight;
import frc.imaging.SimLimelightTarget;
import frc.imaging.SimLimelight.LimelightTargetType;
import frc.io.DriverInput;
import frc.io.RobotOutput;
import frc.io.SensorInput;
import frc.robot.RobotConstants;
import frc.subsystems.Drive;
import frc.subsystems.LEDStrip;
import frc.subsystems.Drive.DriveState;
import frc.subsystems.LEDStrip.LEDColourState;
import frc.util.Debugger;
import frc.util.SimLib;

public class TeleopController extends TeleopComponent {

	private static TeleopController instance;
	private DriverInput driverIn;
	private SensorInput sensorIn;
	private RobotOutput robotOut;
	private Drive drive;
	private LEDStrip led;
	private boolean turningToAngle = false;
	private boolean driverManual = false;
	private boolean operatorManual = false;
	private boolean isPressed = false;
	private SimLimelight limelight;
	private SimLimelightTarget desiredTarget;
	private boolean wasVisionPressed = false;
	private LEDColourState desiredLedState = LEDColourState.OFF;


	public static TeleopController getInstance() {
		if (instance == null) {
			instance = new TeleopController();
		}
		return instance;
	}

	private TeleopController() {
		this.drive = Drive.getInstance();
		this.led = LEDStrip.getInstance();
		this.sensorIn = SensorInput.getInstance();
		this.driverIn = DriverInput.getInstance();
		this.robotOut = RobotOutput.getInstance();
		this.limelight = SimLimelight.getInstance();
	}

	@Override
	public void firstCycle() {
		// TODO Auto-generated method stub
		this.drive.firstCycle();
		
		
	}

	@Override
	public void calculate() {

		////////////////////////////////////
		///////// MISCELLANEOUS CODE ///////
		////////////////////////////////////

		if (this.driverIn.getDriverManualButton()) {
			this.driverManual = true;
		} else if (this.driverIn.getDriverManualntButton()) {
			this.driverManual = false;
		}

		if (this.driverIn.getOperatorManualButton()) {
			this.operatorManual = true;
		} else if (this.driverIn.getOperatorManualntButton()) {
			this.operatorManual = false;
		}
	
		//////////////////////////////
		///////// DRIVE CODE /////////
		//////////////////////////////

		// ΜΑNUAL
		if (driverManual) {

			

		} else {

			
			double x = this.driverIn.getDriverX();
			double y = this.driverIn.getDriverY();

			if (Math.abs(x) < 0.05) {
				x = 0;
			}
			if (Math.abs(y) < 0.1) {
				y = 0;
			}

			x = SimLib.squareMaintainSign(x);
			y = SimLib.squareMaintainSign(y);

		

		}
		this.led.setLed(this.desiredLedState);

		this.drive.calculate();
		
	
	}

	@Override
	public void disable() {
		this.drive.disable();
	
	}
}
