package frc.io;


import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.Spark;


public class RobotOutput {
	private static RobotOutput instance;

	private Compressor compressor;
	private Spark ledStrip;

	private RobotOutput() {
		this.configureSpeedControllers();
		this.compressor = new Compressor();
		this.ledStrip = new Spark(9);
	}

	public static RobotOutput getInstance() {
		if (instance == null) {
			instance = new RobotOutput();
		}
		return instance;
	}

	// Motor Commands

	public void configureSpeedControllers() {

	}


	public void setCompressor(boolean on) {
		if (on) {
			this.compressor.start();
		} else {
			this.compressor.stop();
		}
	}

	public boolean getCompressorState() {
		return this.compressor.enabled();
	}

	public void stopAll() {
		// shut off things here
	}

	public void setLEDStrip(double pattern) {
		ledStrip.set(pattern);
	}

	public double getLEDStrip() {
		return this.ledStrip.get();
	}

}
