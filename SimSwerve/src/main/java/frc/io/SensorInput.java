package frc.io;

import edu.wpi.first.wpilibj.AnalogInput;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class SensorInput {

	private static SensorInput instance;

	private RobotOutput robotOut;


	private DriverStation driverStation;
	private PowerDistributionPanel pdp;
	
	private double lastTime = 0.0;
	private double deltaTime = 20.0;

	private boolean firstCycle = true;

	private DriverStationState driverStationMode = DriverStationState.DISABLED;
	private DriverStation.Alliance alliance;
	private double matchTime;

	private long SystemTimeAtAutonStart = 0;
	private long timeSinceAutonStarted = 0;

	private AnalogInput pressureSensor;


	private SensorInput() {
		this.robotOut = RobotOutput.getInstance();
		this.pdp = new PowerDistributionPanel();
		this.driverStation = DriverStation.getInstance();
		this.pressureSensor = new AnalogInput(0);
	
		this.reset();
	}

	public static SensorInput getInstance() {
		if (instance == null) {
			instance = new SensorInput();
		}
		return instance;
	}

	public void reset() {
		this.firstCycle = true;
		
	}

	public void update() {
		if (this.lastTime == 0.0) {
			this.deltaTime = 20;
			this.lastTime = System.currentTimeMillis();
		} else {
			this.deltaTime = System.currentTimeMillis() - this.lastTime;
			this.lastTime = System.currentTimeMillis();
		}

		if (this.driverStation.isAutonomous()) {
			this.timeSinceAutonStarted = System.currentTimeMillis() - this.SystemTimeAtAutonStart;
			SmartDashboard.putNumber("12_Time Since Auto Start:", this.timeSinceAutonStarted);
		}

		this.alliance = this.driverStation.getAlliance();
		this.matchTime = this.driverStation.getMatchTime();

		if (this.driverStation.isDisabled()) {
			this.driverStationMode = DriverStationState.DISABLED;
		} else if (this.driverStation.isAutonomous()) {
			this.driverStationMode = DriverStationState.AUTONOMOUS;
		} else if (this.driverStation.isOperatorControl()) {
			this.driverStationMode = DriverStationState.TELEOP;
		}
		

	}

	public double getMatchTimeLeft() {
		return this.matchTime;
	}

	public DriverStationState getDriverStationMode() {
		return this.driverStationMode;
	}

	public void resetAutonTimer() {
		this.SystemTimeAtAutonStart = System.currentTimeMillis();
	}


	public double getDeltaTime() {
		return this.deltaTime;
	}

	public enum DriverStationState {
		AUTONOMOUS, TELEOP, DISABLED,
	}

	public DriverStation.Alliance getAllianceColour() {
		return this.alliance;
	}

	public double getMatchTime() {
		return this.matchTime;
	}

	public long getTimeSinceAutoStarted() {
		return this.timeSinceAutonStarted;
	}

	// PDP //

	public double getVoltage() {
		return this.pdp.getVoltage();
	}

	public double getCurrent(int port) {
		return this.pdp.getCurrent(port);
	}

	public double getPressure(){
		return 250 * (pressureSensor.getVoltage()/5.0) - 25;
	}

}