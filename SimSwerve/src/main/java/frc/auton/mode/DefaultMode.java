package frc.auton.mode;

import frc.auton.util.AutonWait;
import frc.auton.util.RunFirstCycle;

/**
 *
 * @author Michael
 */
public class DefaultMode implements AutonMode {

	@Override
	public void addToMode(AutonBuilder ab) {
		// (really, do nothing, it's ok)
		ab.addCommand(new RunFirstCycle());
		// System.out.println("⠀⠀⠀⠀⣀⣤");
		// System.out.println("⠀⠀⠀⠀⣿⠿⣶");
		// System.out.println("⠀⠀⠀⠀⣿⣿⣀");
		// System.out.println("⠀⠀⠀⣶⣶⣿⠿⠛⣶");
		// System.out.println("⠤⣀⠛⣿⣿⣿⣿⣿⣿⣭⣿⣤");
		// System.out.println("⠒⠀⠀⠀⠉⣿⣿⣿⣿⠀⠀⠉⣀");
		// System.out.println("⠀⠤⣤⣤⣀⣿⣿⣿⣿⣀⠀⠀⣿");
		// System.out.println("⠀⠀⠛⣿⣿⣿⣿⣿⣿⣿⣭⣶⠉");
		// System.out.println("⠀⠀⠀⠤⣿⣿⣿⣿⣿⣿⣿");
		// System.out.println("⠀⠀⠀⣭⣿⣿⣿⠀⣿⣿⣿");
		// System.out.println("⠀⠀⠀⣉⣿⣿⠿⠀⠿⣿⣿");
		// System.out.println("⠀⠀⠀⠀⣿⣿⠀⠀⠀⣿⣿⣤");
		// System.out.println("⠀⠀⠀⣀⣿⣿⠀⠀⠀⣿⣿⣿");
		// System.out.println("⠀⠀⠀⣿⣿⣿⠀⠀⠀⣿⣿⣿");
		// System.out.println("⠀⠀⠀⣿⣿⠛⠀⠀⠀⠉⣿⣿");
		// System.out.println("⠀⠀⠀⠉⣿⠀⠀⠀⠀⠀⠛⣿");
		// System.out.println("⠀⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⣿⣿");
		// System.out.println("⠀⠀⠀⠀⣛⠀⠀⠀⠀⠀⠀⠛⠿⠿⠿");
		// System.out.println("⠀⠀⠀⠛⠛");
		//ab.addCommand(new AutonWait(1000));
		
	}
}
