package frc.auton.mode;

import frc.auton.AutonOverride;
import frc.auton.RobotComponent;
import frc.auton.drive.DriveSetOutput;
import frc.auton.drive.DriveSetPosition;
import frc.auton.drive.DriveToVisionTarget;
import frc.auton.drive.DriveTurnToAngle;
import frc.auton.drive.DriveTurnToVisionTarget;
import frc.auton.drive.DriveWait;
import frc.auton.util.AutonWait;

/**
 *
 * @author Michael
 */
public class DriveAtVelocity implements AutonMode {

	@Override
	public void addToMode(AutonBuilder ab) {
        ab.addCommand(new DriveTurnToVisionTarget(15000));
        ab.addCommand(new DriveWait());
    }

}
