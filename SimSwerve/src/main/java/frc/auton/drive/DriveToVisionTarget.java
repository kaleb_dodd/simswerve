package frc.auton.drive;

import frc.auton.AutonCommand;
import frc.auton.RobotComponent;
import frc.imaging.SimLimelight;
import frc.imaging.SimLimelight.LimelightTargetType;
import frc.io.RobotOutput;
import frc.subsystems.Drive;
import frc.subsystems.LEDStrip;
import frc.subsystems.LEDStrip.LEDColourState;

public class DriveToVisionTarget extends AutonCommand {

    private RobotOutput robotOut;
    private Drive drive;
    private LEDStrip led;
    private SimLimelight limelight;
    private boolean isDone;
    private int finishingVisionCycles;
    private double speed;
    private double finishingSpeed;
    private int cyclesUntilDone;

    public DriveToVisionTarget(long timeout) {
        this(5, timeout);
    }

    public DriveToVisionTarget(double speed, long timeout) {
        this(speed, 5, timeout);
    }

    public DriveToVisionTarget(double speed, int cyclesUntilDone, long timeout) {
        this(speed, cyclesUntilDone, 0.06, timeout);
    }

    public DriveToVisionTarget(double speed, int cyclesUntilDone, double finishingSpeed, long timeout) {
        super(RobotComponent.DRIVE, timeout);
        this.led = LEDStrip.getInstance();
        this.robotOut = RobotOutput.getInstance();
        this.drive = Drive.getInstance();
        this.speed = speed;
        this.limelight = SimLimelight.getInstance();
        this.cyclesUntilDone = cyclesUntilDone;
        this.finishingSpeed = finishingSpeed;
    }

    @Override
    public void firstCycle() {
        this.limelight.setLimelight(LimelightTargetType.PANEL_TARGET);
        this.limelight.getTargetInfo();
        this.isDone = false;
        this.finishingVisionCycles = 0;
        this.drive.firstCycle();
    }

    @Override
    public boolean calculate() {
        if (this.limelight.getTargetExists()) {
            if (!this.isDone) {
                this.drive.driveLimeLightXY(this.speed);
                this.led.setLed(this.drive.getDesiredLedState());
            }
            if (drive.getDriveStraightPID().isDone()) {
                this.isDone = true;
            }
        } else {
            this.robotOut.setDriveLeft(0);
            this.robotOut.setDriveRight(0);
            this.led.setLed(LEDColourState.VISION_NOT_AIMED);
        }

        if (this.isDone) {
            this.led.setLed(LEDColourState.VISION_COMPLETE);
            this.finishingVisionCycles++;
            this.robotOut.setDriveLeft(this.finishingSpeed);
            this.robotOut.setDriveRight(this.finishingSpeed);
        }

        
        return this.finishingVisionCycles > this.cyclesUntilDone;
    }

    @Override
    public void override() {
        this.robotOut.setDriveLeft(0);
        this.robotOut.setDriveRight(0);
    }
}
