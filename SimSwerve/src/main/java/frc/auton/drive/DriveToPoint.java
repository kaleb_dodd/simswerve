package frc.auton.drive;

import frc.auton.AutonCommand;
import frc.auton.RobotComponent;
import frc.io.Dashboard;
import frc.io.RobotOutput;
import frc.io.SensorInput;
import frc.robot.RobotConstants;
import frc.subsystems.Drive;
import frc.util.SimLib;
import frc.util.SimPID;
import frc.util.SimPoint;

public class DriveToPoint extends AutonCommand {

	private double x;
	private double y;
	private double theta; // The desired angle
	private double minVelocity;
	private double maxVelocity;
	private double eps; // The acceptable range
	private RobotOutput robotOut;
	private double turnRate;
	private double rampRate;
	private double maxTurn;

	

	private Drive drive;

	public DriveToPoint(double x, double y, double theta, long timeout) {
		this(x, y, theta, 0, 11, 0, 0.25,Dashboard.getInstance().getPathTurnP(), 90, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double eps, long timeout) {
		this(x, y, theta, minVelocity, 11, 0, eps, Dashboard.getInstance().getPathTurnP(), 90, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double eps,
			long timeout) {
		this(x, y, theta, minVelocity, maxVelocity, 0, eps, Dashboard.getInstance().getPathTurnP(), 90, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double eps,
			double turnRate,  long timeout) {
		this(x, y, theta, minVelocity, maxVelocity, 0, eps, turnRate, 90, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double eps,
			double turnRate, double maxTurn, long timeout) {
		this(x, y, theta, minVelocity, maxVelocity, 0, eps, turnRate, maxTurn, timeout);
	}

	public DriveToPoint(double x, double y, double theta, double minVelocity, double maxVelocity, double rampRate,
			double eps, double turnRate, double maxTurn, long timeout) {
		super(RobotComponent.DRIVE, timeout);
		this.x = x;
		this.y = y;
		this.theta = theta;
		this.minVelocity = minVelocity;
		this.maxVelocity = maxVelocity;
		this.eps = eps;
		this.rampRate = rampRate;
		
		this.turnRate = turnRate;
		this.maxTurn = maxTurn;

		this.drive = Drive.getInstance();
		
		this.robotOut = RobotOutput.getInstance();
	}

	@Override
	public void firstCycle() {
		

	}

	@Override
	public boolean calculate() {
		return this.drive.DriveToPoint(x, y, theta, minVelocity, maxVelocity, rampRate, turnRate, maxTurn, eps);
	}

	@Override
	public void override() {

		this.robotOut.setDriveLeft(0);
		this.robotOut.setDriveRight(0);

	}

}
